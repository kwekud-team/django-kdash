from django.urls import reverse
from django.views.generic.edit import FormView
from django.http import HttpResponseRedirect, JsonResponse
from django.contrib import messages

from api.utils.views import ApiFormView
from kdash.views import BaseDashView
from kdash.adminx.utils import get_config_class
from kdash.adminx.views.base import AdminxBase, BaseHandler
from kdash.adminx.constants import KDASH_ADMINX_K


# HANDLERS

class AdminxManageDjangoHandler(BaseHandler):

    def get_filters(self,):
        return extract_form_filters(self.request.GET.items(), form=self.adminx.filter_form)

    def get_queryset(self):
        page = self.request.GET.get('page')
        filters = self.get_filters()
        
        return self.adminx.model_class.objects.filter(**filters)


class AdminxManageRestHandler(BaseHandler):

    def get_object(self):
        return {'pk': self.kwargs.get('pk', None)}
    
    def do_save(self, form_data):
        headers = self.adminx.get_headers(self.request)
        return self.adminx.consumer_class(headers=headers).local_objects.save(
                                        verify_exists=True,
                                        should_update=True,
                                        **form_data)

    def get_form_instance(self, pk, params):
        headers = self.adminx.get_headers(self.request)
        return self.adminx.consumer_class(headers=headers
                                    ).remote_objects.get(pk=pk, params=params) 

    def get_form_params(self, info):
        depth_names = ','.join([x['name'] for x in self.adminx.get_related_fields()])
        return {
            '_depth_names': depth_names,
        }

    def get_extra_initial(self, instance):
        data = {}

        for dt in self.adminx.get_local_fields():
            name = dt['name']
            data[name] = instance.get(name, '')

        for dt in self.adminx.get_related_fields():
            name = dt['name']
            config_path = dt.get('config_path', None)
            remote_text_field = dt.get('remote_text_field', '')

            if config_path:
                field_info = instance.get(name, {}) or {}
                data[name] = field_info.get('pk', None)
                data[name + '__text'] = field_info.get(remote_text_field, '-')

        return data

    def get_default_data(self):
        return {
            'query_data': {},
            'save_data': {}, 
            'related_data': {
                'modified_by': {'username': self.request.user.username},
                'created_by': {'username': self.request.user.username},
            }
        } 

    def get_form_data(self, request, form):
        data = self.get_default_data()

        for dt in self.adminx.get_local_fields():
            name = dt['name']
            value = self.get_value(form, name)['value']
            data['save_data'][name] = (str(value) if value else None)

        for dt in self.adminx.get_related_fields():
            name = dt['name']
            data['save_data'][name] = None
            
            res = self.get_value(form, name)
            if res['value']:
                v = ({'id': res['value']} if res['source'] == 'form' else res['value'])
                data['related_data'][name] = v

        for field in self.adminx.get_query_fields():
            data['query_data'][field] = form.cleaned_data.get(field, None)  

        extra_data = self.adminx.config_class().get_extra_form_data(request, data)

        data.update(extra_data)
        return data

    def get_value(self, form, name):
        func_name = 'get_form_%s' % name
        func = getattr(self.adminx.config_class(), func_name, None)

        if func:
            source = 'function'
            value = func(form, name, request=self.request)
        else:
            source = 'form'
            value = form.cleaned_data.get(name, None)
        
        return {
            'value': value,
            'source': source
        }



## BASE VIEWS ##################################################################

class AdminxManageBase(AdminxBase, ApiFormView):
    admin_type = 'manage'

    handler_classes = {
        KDASH_ADMINX_K['CONFIG_TYPE_DJANGO']: AdminxManageDjangoHandler,
        KDASH_ADMINX_K['CONFIG_TYPE_REST']: AdminxManageRestHandler,
    }

    def get_context_data(self, *args, **kwargs):    
        context = super(AdminxManageBase, self).get_context_data(*args, **kwargs)  
            
        is_popup = self.request.GET.get('_popup', None)
        base_template = ('website/skeleton.html' if is_popup else 'kdash/base.html')

        pk = self.kwargs.get('pk', None)

        context.update({
            'object_pk': pk,
            'base_template_': base_template,
            'is_popup': is_popup,
            'related_fields': [x['name'] for x in self.get_related_fields()],
            'detail_url': self.config_class().get_detail_url(pk, config_path=self.config_path),
        }) 
        return context

    def initialize(self, **kwargs):
        super(AdminxManageBase, self).initialize(**kwargs)

        self.form_class = self.get_form_class()
        self.handler_class = self.get_handler_class(**kwargs)
        self.handler = self.get_handler()
    
    def get_form_class(self, **kwargs):
        return self.config_class.manage_class 

    def get_form_kwargs(self):
        kwargs = super(AdminxManageBase, self).get_form_kwargs()
        kwargs['request'] = self.request
        kwargs['related_fields'] = self.get_related_fields()
        return kwargs

    def get_query_fields(self):
        return self.config_class.manage_fields.get('query', [])

    def get_local_fields(self):
        return self.config_class.manage_fields.get('local', [])

    def get_related_fields(self):
        return self.config_class.manage_fields.get('related', [])

    def get_initial(self):
        dt = {}

        pk = self.kwargs.get('pk', None)
        if pk:
            params = self.get_form_params(pk)
            instance = self.get_form_instance(pk, params)

            dt = self.get_extra_initial(instance)

        return dt

    def get_form_params(self, pk):
        return self.handler.get_form_params(pk)

    def get_form_instance(self, pk, params):
        return self.handler.get_form_instance(pk, params)

    def get_extra_initial(self, instance):
        return self.handler.get_extra_initial(instance)

    def get_form_data(self, request, form):
        return self.handler.get_form_data(request, form)


    def api_form_send_info(self, form):
        form_data = self.get_form_data(self.request, form)
        
        pk = self.kwargs.get('pk', None)
        if pk:
            # We override query data and force searching by id. This ensures
            # we are doing an update, otherwise it will create new object if
            # query does not match
            form_data['query_data'] = {'pk': pk}
            
        return self.handler.do_save(form_data)
        
    def api_form_receive_info(self, api_resp):
        errors = {}
        is_valid = False
        success_url = None
        api_resp = api_resp or {}

        pk = api_resp.get('id', None) 
        if pk:
            is_valid = True
            success_url = self.get_api_success_url(api_resp)
        else:
            errors = {k:v for k, v in api_resp.items()} 
        
        return {
            'success_url': success_url,
            'is_valid': is_valid,
            'errors': errors
        }

    def post(self, request, *args, **kwargs): 
        if request.is_ajax() or request.GET.get('ajax', None):
            self.success_msg = ''

        resp = super(AdminxManageBase, self).post(request, *args, **kwargs)  

        if request.is_ajax() or request.GET.get('ajax', None):
            res = self.process_post_response(self.api_results)
            return JsonResponse(res)

        return resp

    def process_post_response(self, api_results):
        form_errors = api_results.get('form', {}).get('errors', {})

        if form_errors:
            is_valid = False
            resp_code = 0
            resp_message = ', '.join(['%s: %s' % (k, ', '.join(v)) for k,v in form_errors.items()])

        else:
            is_valid = api_results.get('form', {}).get('processed_results', {}).get('is_valid', False)
            resp_code = api_results.get('form', {}).get('api_response', {}).get('pk', -1)
            resp_message = api_results.get('form', {}).get('api_response', {}).get('name', '')

            if not is_valid:
                form_errors = api_results.get('form', {}).get('api_response', {})
                resp_message = ', '.join(['%s: %s' % (k, ', '.join(v)) for k,v in form_errors.items()])

        return {
            'is_valid': is_valid,
            'resp_code': resp_code,
            'resp_message': resp_message,
        }

    def get_api_success_url(self, info):
        pk = info.get('pk', None)
        if pk:
            url = self.config_class().get_detail_url(pk, config_path=self.config_path)
        else:
            url = self.config_class().get_list_url(config_path=self.config_path)
        return url


class ManageDashView(AdminxManageBase, BaseDashView):
    pass





class AdminxDeleteDjangoHandler(BaseHandler):

    def delete_object(self):
        aaa


class AdminxDeleteRestHandler(BaseHandler):

    def delete_object(self):
        pk = self.adminx.kwargs.get('pk', None)
        headers = self.adminx.get_headers(self.request)

        return self.adminx.consumer_class(headers=headers
                                          ).local_objects.delete(pk=pk)
    


class DeleteDashView(AdminxBase):
    handler_classes = {
        KDASH_ADMINX_K['CONFIG_TYPE_DJANGO']: AdminxDeleteDjangoHandler,
        KDASH_ADMINX_K['CONFIG_TYPE_REST']: AdminxDeleteRestHandler,
    }

    def get(self, request, *args, **kwargs):
        is_deleted = self.handler.delete_object()
        if is_deleted:
            messages.success(request, 'Deleted successfully')

        url = self.config_class().get_list_url(config_path=self.config_path)
        return HttpResponseRedirect(url)
