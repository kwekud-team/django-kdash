# from commons.utils.filters import extract_form_filters, get_order_fields
from kommons.utils.excel import TablibExcelExporterWT
from django.template.defaultfilters import strip_tags

from kommons.utils.filters import RequestFilters
from kommons.utils.model import get_str_from_model
from kdash.views import BaseDashView
from kdash.adminx.views.base import AdminxBase, BaseHandler
from kdash.adminx.constants import KDASH_ADMINX_K
from kdash.adminx.forms import GenericSortForm
from kdash.adminx.utils import get_recursive_attr


# HANDLERS ####################################################################
class AdminxListDjangoHandler(BaseHandler):

    def get_filters(self):
        filters = self.adminx.config_class.list_filters.copy()
        # req_filters = extract_form_filters(self.request.GET.items(), form=self.adminx.filter_form)
        req_filters = {}
        filters.update(req_filters)
        # return filters
        return self.adminx.config_class().get_extra_filters(self.request, filters)

    def get_sub_filters(self):
        filters = {}
        for x in self.adminx.config_class().sub_filter_classes:
            app_model = get_str_from_model(x._meta.model)
            form = x(self.request.GET, request=self.request).form
            filters[app_model] = RequestFilters(self.request.GET, form=form).gen_filters()

        return filters

    def get_queryset(self):
        filters = self.get_filters()
        sub_filters = self.get_sub_filters()

        qs = self.adminx.config_class().get_extra_queryset(self.request, filters, sub_filters=sub_filters)
        if self.adminx.config_class().filter_class:
            qs = self.adminx.config_class().filter_class(self.request.GET, queryset=qs, request=self.request).qs
        return qs


class AdminxListRestHandler(BaseHandler):
    
    def get_filters(self):
        filters = self.adminx.config_class.list_filters.copy()

        order_fields = self.adminx.get_order_fields()
        filters.update({'ordering': ','.join(order_fields)})
        
        req_filters = {}
        filters.update(req_filters)
        
        return filters

    def get_queryset(self):
        page = self.request.GET.get('page')
        params = self.get_filters()
        headers = self.adminx.get_headers(self.request)
        
        return self.adminx.consumer_class(paginate_by=self.adminx.get_paginate_by(), headers=headers)\
            .local_objects.paginated(page=page, params=params)


# BASE VIEWS ##################################################################
class AdminxListBase(AdminxBase):
    admin_type = 'listing'
    sort_class = GenericSortForm

    handler_classes = {
        KDASH_ADMINX_K['CONFIG_TYPE_DJANGO']: AdminxListDjangoHandler,
        KDASH_ADMINX_K['CONFIG_TYPE_REST']: AdminxListRestHandler,
    }

    def export_as_excel(self):
        fields = self.get_fields()['list_fields']
        headers = [x['label'] for x in fields]
        field_names = [x['attribute'] for x in fields]

        data = []
        qs = self.get_queryset()
        for obj in qs:
            tmp = []
            for f in field_names:
                val = get_recursive_attr(obj, f.split('.'))
                tmp.append(strip_tags(val))
            data.append(tmp)

        summary = self.get_summary(qs)
        if summary:
            data.append([])
            data.append(['Summary'] + [''] * (len(headers) - 1))

            for obj in summary:
                data.append([obj['label'], obj['value']] + [''] * (len(headers) - 2))

        filename = str(self.model_class._meta.verbose_name_plural)
        exporter = TablibExcelExporterWT(filename=filename)

        info = {
            'headers': headers,
            'data': data,
            'title': filename.upper()
        }
        sheet = exporter.build_sheet(info)
        dataset = exporter.build_dataset([sheet])
        return exporter.export(dataset)

    def get(self, request, *args, **kwargs):
        rformat = request.GET.get('format', None)
        if rformat == 'excel':
            return self.export_as_excel()

        return super(AdminxListBase, self).get(request, *args, **kwargs)

    def get_context_data(self, *args, **kwargs):    
        context = super(AdminxListBase, self).get_context_data(*args, **kwargs)  

        queryset = self.get_queryset()

        context.update({ 
            'sort_form': self.get_sort_form(),
            'filter_layout': self.get_filter_layout(),
            'summary_layout': self.get_summary_layout(),
            'filter_options_form': self.get_filter_options_form(),
            'filter_form': self.get_filter_form(),
            'sub_filter_forms': self.get_sub_filter_forms(),
            'add_url': self.config_class().get_add_url(),
            'object_list': queryset,
            'summary': self.get_summary(queryset),
            'paginate_by': self.get_paginate_by(),
            'fields': self.get_fields(),
            'page_start_index': self.get_page_start_index(),
            'actions': self.get_actions(),
            'list_page_actions': self.get_list_page_actions(),

            'export_as_excel': self.config_class.export_as_excel
        })

        return context

    def initialize(self, **kwargs):
        super(AdminxListBase, self).initialize(**kwargs)

        self.filter_class = self.get_filter_class(**kwargs)
        self.sub_filter_classes = self.get_sub_filter_classes(**kwargs)
        self.filter_form = self.get_filter_form()
        self.sort_form = self.get_sort_form()

    def get_paginate_by(self):
        return self.config_class.paginate_by

    def get_filter_layout(self, **kwargs):
        return self.config_class.filter_layout

    def get_summary_layout(self, **kwargs):
        return self.config_class.summary_layout

    def get_filter_class(self, **kwargs):
        return self.config_class.filter_class

    def get_sub_filter_classes(self, **kwargs):
        return self.config_class.sub_filter_classes

    def get_filter_options_form(self, **kwargs):
        form_class = self.config_class.filter_options_class
        if form_class:
            filter_formats = self.config_class.filter_formats
            return form_class(self.request.GET,
                              paginate_by=self.config_class.paginate_by,
                              filter_formats=filter_formats)

    def get_sort_form(self, **kwargs):
        sort_fields = self.config_class.sort_fields
        if sort_fields:
            return self.sort_class(self.request.GET, sort_fields=sort_fields)

    def get_filter_form(self, **kwargs):
        if self.filter_class:
            return self.filter_class(self.request.GET, request=self.request).form

    def get_sub_filter_forms(self):
        xs = []
        for x in self.sub_filter_classes:
            xs.append(x(self.request.GET, request=self.request).form)
        return xs

    def get_order_fields(self):
        return get_order_fields(self.request) 

    def get_queryset(self):
        raise NotImplementedError('Implement by child classes')

    def get_summary(self, queryset):
        return self.config_class().get_summary(self.request, queryset)

    def get_list_fields(self):
        raw_fields = self.config_class.list_fields
        return self.process_fields(raw_fields)

    def get_detail_fields(self):
        raw_fields = self.config_class.list_summary_fields

        if raw_fields is None:
            # User explicitly does not want summary fields
            raw_fields = []
            
        elif raw_fields is []:
            # Defaults to base class empty list. we look to detail fields for value
            raw_fields = self.config_class.detail_fields
        
        xs = []
        for x in raw_fields:
            xs.append(
                (x[0] or '-', self.process_fields(x[1]))
            )

        return xs

    def get_fields(self):
        return {
            'list_fields': self.get_list_fields(),
            'detail_fields': self.get_detail_fields()
        }

    def get_filters(self):
        return {}

    # def get_queryset(self):
    #     return []

    def get_page_start_index(self):
        page_str = self.request.GET.get('page')
        try:
            page = int(str(page_str))
        except ValueError:
            page = 1

        return (page - 1) * self.get_paginate_by()
        
    def get_actions(self):
        actions = self.config_class.actions
        if actions is not None:  # User explicitly does not want summary fields
            if self.config_class.can_change:
                actions.append({
                    'label': 'Change',
                    'base': 'kdash_adminx:change',
                    'kwargs': {'config_path': self.config_path},
                    'future_kwargs': {'pk': 'pk'},
                    'icon': 'pencil text-warning'
                })

            if self.config_class.can_delete:
                actions.append({
                    'label': 'Delete',
                    'base': 'kdash_adminx:delete',
                    'kwargs': {'config_path': self.config_path},
                    'future_kwargs': {'pk': 'pk'},
                    'icon': 'trash text-danger',
                    'class': 'delete-popup-link'
                })
        return actions

    def get_list_page_actions(self):
        actions = self.config_class.list_page_actions
        return actions


# Views #######################################################################

class ListDashView(AdminxListBase, BaseDashView):

    def get_queryset(self): 
        return self.handler.get_queryset()


class CentralDashView(AdminxBase, BaseDashView):
    admin_type = 'central'

    def get_context_data(self, **kwargs):
        context = super(CentralDashView, self).get_context_data(**kwargs)

        dash_info = context.get('dash_info', {}).get('current_dash', {})
        context.update({
            'child_list': self.get_child_list(dash_info),
        })

        return context

    def get_child_list(self, dash_info):
        qs = self.dash_utils.get_dash_queryset().filter(parent__pk=dash_info.get('pk', None))
        return self.dash_utils.get_dash_info(qs)
