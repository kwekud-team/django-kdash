from kdash.adminx.views.listing import ListDashView
from django.db import models


class ReportBaseView(ListDashView):
    def get_paginate_by(self):
        from_default = self.config_class.paginate_by
        from_request = self.request.GET.get('paginate_by', None)
        val = from_request or from_default
        return int(val)


class ReportListView(ReportBaseView):
    pass


class ReportSummaryView(ReportBaseView):
    pass
