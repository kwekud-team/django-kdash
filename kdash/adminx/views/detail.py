from django.urls import reverse

from kdash.views import BaseDashView
from kdash.adminx.views.base import AdminxBase, BaseHandler
from kdash.adminx.constants import KDASH_ADMINX_K


## HANDLERS ####################################################################

class AdminxDetailDjangoHandler(BaseHandler):

    def get_object(self, pk):
        return self.adminx.model_class.objects.get(pk=pk)


class AdminxDetailRestHandler(BaseHandler):

    def get_filters(self):
        return self.adminx.config_class.list_filters

    def get_object(self, pk):
        params = self.get_filters()
        headers = self.adminx.get_headers(self.request)

        return self.adminx.consumer_class(headers=headers
                                ).local_objects.get(pk=pk, params=params)



## BASE VIEWS ##################################################################

class AdminxDetailBase(AdminxBase):
    admin_type = 'detail'

    handler_classes = {
        KDASH_ADMINX_K['CONFIG_TYPE_DJANGO']: AdminxDetailDjangoHandler,
        KDASH_ADMINX_K['CONFIG_TYPE_REST']: AdminxDetailRestHandler,
    }

    def get_context_data(self, *args, **kwargs):    
        context = super(AdminxDetailBase, self).get_context_data(*args, **kwargs)  
        
        pk = self.kwargs['pk']
        
        context.update({ 
            'add_url': self.config_class().get_add_url(),
            'change_url': self.config_class().get_change_url(pk),
            'delete_url': self.config_class().get_delete_url(pk),
            'fields': self.get_fields(),
            'actions': self.get_actions(),
            'object': self.get_object()
        })  
        return context

    def initialize(self, **kwargs):
        super(AdminxDetailBase, self).initialize(**kwargs)

    def get_detail_fields(self):
        raw_fields = self.config_class.list_summary_fields

        if raw_fields is None:
            # User explicity does not want summary fields
            raw_fields = []
            
        elif raw_fields == []:
            # Defaults to base class empty list. we look to detail fields for value
            raw_fields = self.config_class.detail_fields
        
        xs = []
        for x in raw_fields:
            xs.append(
                (x[0] or '-', self.process_fields(x[1]))
            )

        return xs

    def get_fields(self):
        return {
            'detail_fields': self.get_detail_fields()
        }

    def get_actions(self):
        urls = [
            {
                'label': 'Change', 
                'base': 'kdash_adminx:change', 
                'kwargs': {'config_path': self.config_path}, 
                'future_kwargs': {'pk': 'pk'}
            }
        ]
        return urls

    def get_object(self):
        pk = self.kwargs['pk']
        return self.handler.get_object(pk)


## Views #######################################################################

class DetailDashView(AdminxDetailBase, BaseDashView):
    pass