from django.urls import reverse
from django.views.generic import TemplateView
from django.http import Http404
from django.db import models
from django.template.defaultfilters import floatformat
from django.contrib.humanize.templatetags.humanize import intcomma

from kdash.adminx.utils import get_config_class
from kdash.adminx.constants import KDASH_ADMINX_K, KdashAdminxK
from kdash.adminx.forms import FilterOptionsForm


class AdminxBase(TemplateView):
    handler_classes = {}
    admin_type = 'base'
    
    def dispatch(self, request, *args, **kwargs):
        self.initialize(**kwargs)
        return super(AdminxBase, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, *args, **kwargs):    
        context = super(AdminxBase, self).get_context_data(*args, **kwargs)  
                 
        context.update({ 
            'config_path': self.config_path,
            'config_class': self.get_config_class()
        }) 

        context.update(self.get_config_context(self.request, **kwargs))

        return context

    def initialize(self, **kwargs):
        self.workspace = self.request.workspace
        
        self.config_path = kwargs['config_path']
        try:
            self.config_class = self.get_config_class(**kwargs)
        except ImportError:
            raise Http404('Resource not found: %s. Check the path of the kdash config file' % self.config_path)

        self.config_class_type = self.get_config_class_type(**kwargs)
        self.handler_class = self.get_handler_class(**kwargs)
        self.handler = self.get_handler()

        if self.config_class_type == KDASH_ADMINX_K['CONFIG_TYPE_DJANGO']:
            self.model_class = self.get_model_class(**kwargs)

        if self.config_class_type == KDASH_ADMINX_K['CONFIG_TYPE_REST']:
            self.consumer_class = self.get_consumer_class(**kwargs)

    def get_handler_class(self, **kwargs):
        return self.handler_classes.get(self.config_class_type, None)

    def get_handler(self):
        if self.handler_class:
            return self.handler_class(adminx=self, request=self.request)

    def get_config_class(self, **kwargs):
        return get_config_class(self.config_path)

    def get_config_class_type(self, **kwargs):
        return self.config_class._config_type

    def get_consumer_class(self, **kwargs):
        return self.config_class.consumer_class 

    def get_model_class(self, **kwargs):
        return self.config_class.model_class

    def process_fields(self, raw_fields):
        xs = []
        for f in raw_fields:
            if type(f) == str:
                f = {'name': f}
                
            name = f['name']
            name_label = ' '.join(name.split('_')).capitalize()

            attribute = f.get('attribute', name)
            label = f.get('label', name_label)

            xs.append({
                'name': name, 
                'label': label, 
                'attribute': attribute
            })
        return xs

    def get_template_names(self):
        """ Get possible template names in this order
            1. Custom template defined in attribute in config class
            2. Custom template under platform folder and app and model name
            3. Custom template under app folder
            4. Default template defined under AdminxBase class

            Eg: If platform==hr, app=='entities', model='supplier'
            1. hr/registered_supplier.html
            2. hr/listing_entities_supplier.html
            3. hr/listing.html
            4. kdash_adminx/listing.html
        """
        templates = []
        
        # 1
        if self.config_class.listing_template:
            templates.append(self.config_class.listing_template)

        app_components = self.get_app_components()
        app_components.update({'admin_type': self.admin_type})

        # 2
        plat_app_model = '%(platform)s/%(admin_type)s_%(app)s_%(model)s.html' % app_components
        templates.append(plat_app_model)
        
        # 3 
        plat_only = '%(platform)s/%(admin_type)s.html' % app_components
        templates.append(plat_only)

        # 4
        def_tempt = 'kdash_adminx/%(admin_type)s.html' % app_components
        templates.append(def_tempt)
        
        return templates

    def get_config_context(self, request, **kwargs):
        return self.config_class().get_extra_context(request, **kwargs)

    def get_dash_name(self):
        return self.config_path 

    def get_app_components(self):
        platform_name, app_model = self.config_path.split(':')
        section = ''
        if '-' in app_model:
            app_model, section = app_model.split('-')

        app, model = app_model.split('.')

        return {
            'platform': platform_name,
            'app': app,
            'model': model,
            'section': section
        }

    def get_headers(self, request, keys=[]):
        keys = keys or [KDASH_ADMINX_K['HEADER_KEY_AUTH_TOKEN'], 
                        KDASH_ADMINX_K['HEADER_KEY_WORKSPACE']]
        headers = {}

        if KDASH_ADMINX_K['HEADER_KEY_AUTH_TOKEN'] in keys:
            if request.user.is_authenticated:
                profile = request.user.profile
                headers.update({
                    "Authorization": "Token %s" % profile.description
                })

        # if KDASH_ADMINX_K['HEADER_KEY_WORKSPACE'] in keys:
        #     from transact.ega.utils import get_user_details
        #
        #     user_details = get_user_details(request)
        #     workspace_dt = user_details.get('workspace', {}).get('current', {})
        #     workspace_guid = workspace_dt.get('workspace__guid', None)
        #     if workspace_guid:
        #         headers.update({
        #             "WORKSPACEGUID": workspace_guid
        #         })
        
        return headers
        

class BaseConfig(object):
    config_path = ''
    manage_class = None
    filter_class = None
    lookup_class = None

    paginate_by = 20
    can_view_detail = True
    can_add = True
    can_change = True
    can_delete = True
    can_filter = True
    can_sort = True

    sort_fields = []
    list_fields = []
    list_summary_fields = []  # Used on listing for quick detail popup page
    detail_fields = []
    list_filters = {}
    manage_fields = {}
    list_page_actions = []
    actions = []  # TODO: Rename to list_row_actions

    listing_template = ''
    detail_template = ''
    manage_template = ''

    # Reports
    filter_options_class = FilterOptionsForm
    filter_formats = (('preview', 'Preview'), ('excel', 'Excel'))
    filter_layout = KdashAdminxK.FILTER_LAYOUT_AS_MODAL.value
    summary_layout = KdashAdminxK.SUMMARY_LAYOUT_MODAL.value
    sub_filter_classes = []
    report_summary_fields = []
    export_as_excel = False

    def get_list_url(self, **kwargs): 
        config_path = kwargs['config_path']
        return reverse('kdash_adminx:list', args=(config_path,))

    def get_add_url(self, **kwargs): 
        config_path = kwargs.get('config_path', self.config_path)
        if self.can_add: 
            return reverse('kdash_adminx:add', args=(config_path,))
    
    def get_change_url(self, pk, **kwargs):
        config_path = kwargs.get('config_path', self.config_path) 
        if self.can_change: 
            return reverse('kdash_adminx:change', args=(config_path, pk))

    def get_delete_url(self, pk, **kwargs):
        config_path = kwargs.get('config_path', self.config_path) 
        if self.can_delete: 
            return reverse('kdash_adminx:delete', args=(config_path, pk))

    def get_detail_url(self, pk, **kwargs):
        if pk:
            config_path = kwargs.get('config_path', self.config_path) 
            return reverse('kdash_adminx:detail', args=(config_path, pk))

    def get_extra_context(self, *args, **kwargs):
        return {
            'config_perms': {
                'can_view_detail': self.can_view_detail,
                'can_add': self.can_add,
                'can_change': self.can_change,
                'can_delete': self.can_delete,
                'can_filter': self.can_filter,
            }
        }

    def get_extra_form_data(self, request, data, **kwargs):
        return {}


class BaseDjangoConfig(BaseConfig):
    _config_type = KDASH_ADMINX_K['CONFIG_TYPE_DJANGO']

    model_class = None

    def get_extra_filters(self, request, filters):
        return filters

    def get_extra_queryset(self, request, filters, sub_filters=None):
        return self.model_class.objects.filter(**filters)

    def get_summary(self, request, queryset):
        xs = []

        for x in self.report_summary_fields:
            val = queryset.aggregate(val=models.Sum(x['name']))['val']
            val = intcomma(floatformat(val, 2))
            xs.append({
                'name': x['name'],
                'label': x['label'],
                'value': val
            })
        return xs


class BaseRestConfig(BaseConfig):
    _config_type = KDASH_ADMINX_K['CONFIG_TYPE_REST']

    consumer_class = None


class BaseDjangoReportConfig(BaseDjangoConfig):
    filter_layout = KdashAdminxK.FILTER_LAYOUT_AS_SIDEBAR.value


class BaseHandler(object):
    
    def __init__(self, adminx, request):
        self.adminx = adminx
        self.request = request
