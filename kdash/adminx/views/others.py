from django.urls import reverse
from django.views.generic import View
from django.http import HttpResponseRedirect

from dal import autocomplete
from kdash.adminx.utils import get_config_class
from kdash.adminx.views.base import AdminxBase


class ConsumerRestSearchDAL(autocomplete.Select2QuerySetView):

    def dispatch(self, request, *args, **kwargs):
        self.config_class = get_config_class(kwargs['config_path'])
        self.lookup_view = self.config_class.lookup_class()
        return super(ConsumerRestSearchDAL, self).dispatch(request, *args, **kwargs)

    # def has_more(self, context): 
    #     return context['page_obj'].has_next()

    def get_result_value(self, result):
        return str(result['pk']) 

    def get_result_label(self, item):
        return self.lookup_view.get_result_label(self, item)

    def get_extra_params(self):
        return self.lookup_view.get_extra_params(self)

    def get_queryset(self):
        params = {'_search': self.q}
        params.update(self.get_extra_params())
        headers = AdminxBase().get_headers(self.request)

        consumer_class = self.config_class.consumer_class
        res = consumer_class(headers=headers).local_objects.filter(params=params)
        return res.get('results', [])


class PopupRelatedFormView(View):

    def get_url(self):
        config_path = self.request.GET.get('config_path', '')
        self.config_class = get_config_class(config_path)

        pk = self.request.GET.get('pk', '')
        params = self.request.GET.get('params', '')
        
        config_obj = self.config_class()

        if pk:
            url = config_obj.get_change_url(pk)
        else:
            url = config_obj.get_add_url()

        url += '?_popup=1&' + params

        return url
        
    def get(self, request, *args, **kwargs):
        url = self.get_url()
        return HttpResponseRedirect(url)

    def post(self, request, *args, **kwargs):
        url = self.get_url()
        return HttpResponseRedirect(url)
