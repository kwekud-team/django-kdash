from enum import Enum


KDASH_ADMINX_K = {
    'CONFIG_TYPE_DJANGO': 'config_type_django',
    'CONFIG_TYPE_REST': 'config_type_rest',

    'HEADER_KEY_AUTH_TOKEN': 'header_key_auth_token',
    'HEADER_KEY_WORKSPACE': 'header_key_workspace',
}


class KdashAdminxK(Enum):
    FILTER_LAYOUT_AS_MODAL = 'filter-layout-as-modal'
    FILTER_LAYOUT_AS_SIDEBAR = 'filter-layout-as-sidebar'

    SUMMARY_LAYOUT_MODAL = 'summary-layout-modal'
    SUMMARY_LAYOUT_SIDEBAR = 'summary-layout-sidebar'
    SUMMARY_LAYOUT_TOP = 'summary-layout-top'
