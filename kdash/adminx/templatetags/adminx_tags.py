from django import template
from django.utils.safestring import mark_safe
from kdash.adminx.utils import get_recursive_attr

register = template.Library()


@register.simple_tag(takes_context=True)
def get_config_value(context, obj, config_class, field_name):
    if field_name.startswith('get_this_') and hasattr(config_class, field_name):
        request = context['request']
        return getattr(config_class, field_name)(request, obj)

    return get_recursive_attr(obj, field_name.split('.'))
