from django import forms

# from betterforms.forms import BetterForm


class AbstractManageForm(forms.Form):

    # Use Attr class so we don't conflict with django Meta
    # class Attr:
    #     fields = []

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        self.related_fields = kwargs.pop('related_fields', [])
        super(AbstractManageForm, self).__init__(*args, **kwargs)
        
        # set fields to use bootstrap
        for field_name in self.fields:
            self.fields[field_name].widget.attrs.update({
                        'class': 'form-control form-control-sm straight-edges'
                    })

        # Field initial values fromm GET parameters
        for field_name in self.fields:
            value = self.request.GET.get(field_name, None)
            if value:
                self.fields[field_name].initial = value

        # Set data attribute to display change, add icons next to fields on form
        for dt in self.related_fields:
            name = dt['name']
            config_path = dt.get('config_path', None)
            if config_path and name in self.fields:
                self.fields[name].widget.attrs.update({
                                'popup_related_config_path': config_path,
                                'popup_related_params': dt.get('params', '')
                            })

        self.related_init_values()

    def related_init_values(self):
        for x in self.related_fields:
            name = x['name']

            if name in self.fields:
                # Set initial values from rest data
                if name in self.initial:
                    pk = self.initial.get(name, '')
                    text = self.initial.get('%s__text' % name, '')

                    xs = [(pk, text)] 
                    self.fields[name].choices = xs

                # Set post data to choices, otherwise it will fail form validation
                if self.request.method == 'POST':
                    pk = self.request.POST.get(name, None)
                    xs = [(pk, '---')]
                    self.fields[name].choices = xs


SORT_DIRECTIONS = (
    ('asc', 'Ascending'),
    ('desc', 'Descending'),
)


class GenericSortForm(forms.Form):
    sort_field_name = forms.ChoiceField(required=False, choices=[], label='Field')
    sort_direction = forms.ChoiceField(required=False, choices=SORT_DIRECTIONS, label='Direction')

    def __init__(self, *args, **kwargs):
        sort_fields = kwargs.pop('sort_fields', [])
        super(GenericSortForm, self).__init__(*args, **kwargs)

        choices = []
        for x in sort_fields:
            choices.append((x['name'], x['label']))

        self.fields['sort_field_name'].choices = choices


PAGINATE_BY = (
    (10, '10'),
    (25, '25'),
    (50, '50'),
    (100, '100'),
)


class FilterOptionsForm(forms.Form):
    format = forms.ChoiceField(required=False, choices=[], label='Format',
                               widget=forms.Select(attrs={'class': 'noESelect'}))
    paginate_by = forms.ChoiceField(required=False, choices=PAGINATE_BY, label='Paginate by',
                                    widget=forms.Select(attrs={'class': 'noESelect'}))

    def __init__(self, *args, **kwargs):
        filter_formats = kwargs.pop('filter_formats', [])
        paginate_by = kwargs.pop('paginate_by', None)
        super(FilterOptionsForm, self).__init__(*args, **kwargs)

        if paginate_by:
            self.fields['paginate_by'].initial = paginate_by

        self.fields['format'].choices = tuple(filter_formats)
