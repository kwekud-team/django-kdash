from django.urls import path

from kdash.adminx import views
from kdash.adminx.views import listing, detail, manage  #, others
from kdash.adminx.views.reports import listing as report_listing

app_name = 'kdash_adminx'


urlpatterns = [  
    # path('autocomplete/consumer-rest/<str:config_path>/', views.others.ConsumerRestSearchDAL.as_view(),
    #      name='autocomplete_consumer_rest'),
    # path('popup-related/controller/', views.others.PopupRelatedFormView.as_view(), name='popup_related_controller'),

    path('central/<str:config_path>/', views.listing.CentralDashView.as_view(), name='central'),
    path('list/<str:config_path>/', views.listing.ListDashView.as_view(), name='list'),
    path('add/<str:config_path>/', views.manage.ManageDashView.as_view(), name='add'),
    path('change/<str:config_path>/<str:pk>/', views.manage.ManageDashView.as_view(), name='change'),
    path('delete/<str:config_path>/<str:pk>/', views.manage.DeleteDashView.as_view(), name='delete'),
    path('detail/<str:config_path>/<str:pk>/', views.detail.DetailDashView.as_view(), name='detail'),

    path('reports/list/<str:config_path>/', report_listing.ReportListView.as_view(), name='report_listing'),
    path('reports/summary/<str:config_path>/', report_listing.ReportSummaryView.as_view(), name='report_summary'),
]
