
function popupDelete(){
    
    $('.delete-popup-link').click(function(event) {
        event.preventDefault();

        var url = $(this).attr('href');

        bootbox.confirm({
            message: "Are you sure you want to delete this record?",
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result){
                    window.location.href = url;
                }
            }
        });

    });
    
}