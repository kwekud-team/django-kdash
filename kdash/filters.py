import django_filters


# class SampleFilterForm(django_filters.FilterSet):
#     pk = django_filters.CharFilter()


class MyDateRangeWidget(django_filters.widgets.RangeWidget):
    suffixes = ['_gte', '_lte']


CHOICES_IS_NULL = (
    ('', '---'),
    ('0', 'Yes'),
    ('1', 'No'),
)

CHOICES_YES_NO = (
    ('', '---'),
    ('1', 'Yes'),
    ('0', 'No'),
)