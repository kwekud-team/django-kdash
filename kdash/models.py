# -*- coding: utf-8 -*-s
from django.db import models  
from django.contrib.sites.managers import CurrentSiteManager
from mptt.models import MPTTModel, TreeForeignKey
from mptt.managers import TreeManager


class DashboardItem(MPTTModel, models.Model):
    site = models.ForeignKey('sites.Site', on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    label = models.CharField(max_length=100)
    content_str = models.CharField(max_length=100, null=True, blank=True)
    function_path = models.CharField(max_length=100, null=True, blank=True)
    description = models.CharField(max_length=250, null=True, blank=True)
    in_menu = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    sort = models.PositiveIntegerField(default=1000)
    parent = TreeForeignKey('self', null=True, blank=True, related_name='children', 
                            on_delete=models.CASCADE)
    url_name = models.CharField(max_length=100)
    icon_class = models.CharField(max_length=100, null=True, blank=True)
    ttype = models.CharField(max_length=100, null=True, blank=True, default='regular')

    tree = TreeManager()
    objects = models.Manager()
    on_site = CurrentSiteManager()

    def __str__(self):
        return '%s: %s' % (self.content_str, self.name)

    class Meta:
        ordering = ('sort',)
        unique_together = ('content_str', 'name', 'site', 'ttype')

    class MPTTMeta:
        order_insertion_by = ['sort'] 

    def get_compound_name(self):
        return self.name
