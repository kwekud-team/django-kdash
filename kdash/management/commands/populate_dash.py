from django.core.management.base import BaseCommand
from django.conf import settings

from django.contrib.sites.models import Site
from kdash.registry import Register


class Command(BaseCommand):
    help = 'Populates DashboardItem table with define dash items'

    def add_arguments(self, parser):
        parser.add_argument('site_id', type=int, nargs='?', default=0)

    def handle(self, *args, **options):
        site_id = options['site_id']
        if not site_id:
            site_id = settings.SITE_ID

        site = Site.objects.get(id=site_id)
        
        Register.populate(site)
        Register.create_permissions(site)
        self.stdout.write(self.style.SUCCESS('Populated dashboard items successfully'))
