from django.contrib.auth.models import Permission
from django.contrib.contenttypes.models import ContentType
from django.conf import settings
from django.contrib.sites.shortcuts import get_current_site
from django.apps import apps
from django.urls import reverse
from django.conf import settings

from kommons.utils.model import get_str_from_model
from kommons.utils.core import dotted_import
from kdash.models import DashboardItem
from kdash.constants import KdashK


def get_perm_codename(dash_item):
    return f'{KdashK.PERM_PREFIX.value}_{dash_item._meta.model_name}|{dash_item.name}'


def create_dash_perm(perm_prefix, dash_item): 
    content_type = ContentType.objects.get_for_model(DashboardItem)

    # 'view_dashboarditem:accounting.billitem'
    # codename = '%s_%s' % (perm_prefix, dash_item.name)
    # codename = f'{perm_prefix}_{dash_item._meta.model_name}|{dash_item.name}'
    codename = get_perm_codename(dash_item)
    name = 'View DashItem: %s' % dash_item.name.replace('_', ' ')
    Permission.objects.get_or_create(
                    codename=codename,
                    content_type=content_type,
                    defaults={
                        'name': name.title()
                    })


class DashUtils:
    dash_info = {}
    page_title = None
    page_subtitle = None
    site = None
    frontend_base_template = 'bs4_classic/placeholders/cols_2x10.html'

    def __init__(self, request, site, dash_name='', page_title=None, page_subtitle=None):
        self.request = request
        self.site = site
        self.page_title = page_title
        self.page_subtitle = page_subtitle
        self.dash_name = dash_name

    def get_site(self):
        kdash_site = getattr(settings, 'KDASH_SITE_DOMAIN', None)
        if kdash_site:
            from django.contrib.sites.models import Site

            site = Site.objects.filter(domain=kdash_site).first()
            if site:
                return site

        return self.site

    def get_context(self):
        return {
            'kperms_installed': apps.is_installed("kperms"),
            'page_title': self.page_title,
            'page_subtitle': self.page_subtitle,
            'dash_info': self.get_dash_info(self.get_dash_queryset()) or {},
            'base_template': getattr(settings, 'KDASH_BASE_TEMPLATE', self.frontend_base_template),
        }

    def get_dash_queryset(self):
        return DashboardItem.objects.filter(site=self.get_site(), is_active=True)

    def get_dash_info(self, queryset):
        all_dashes = self.get_dash_xs(queryset)

        curr_dash = None
        for x in all_dashes:
            curr_dash = self.get_dash_current(x)
            if curr_dash:
                break

        if not curr_dash and all_dashes:
            curr_dash = all_dashes[0]

        menu_queryset = queryset.filter(parent__isnull=True, in_menu=True)
        visible_dashes = self.get_dash_xs(menu_queryset)

        return {
            'all_dashes': all_dashes,
            'visible_dashes': visible_dashes,
            'current_dash': curr_dash
        }

    def get_dash_current(self, dash_item):
        curr_dash = None

        if dash_item.get('is_active', False):
            curr_dash = dash_item

        elif dash_item.get('is_parent_active', False):
            for x in dash_item['children']:
                curr_dash = self.get_dash_current(x)
                if curr_dash:
                    break

        return curr_dash

    def get_dash_xs(self, queryset):
        xs = []
        for x in queryset:
            dt = self.get_dash_dt(x)
            if dt:
                children_qs = x.children.filter(is_active=True)
                if children_qs.exists():
                    dt['children'] = self.get_dash_xs(children_qs)

                xs.append(dt)

        return xs

    def _get_dash_menu_label(self, dash_item):
        if dash_item.ttype == KdashK.DASH_TYPE_CUSTOM_MENU_LABEL.value and dash_item.function_path:
            function = dotted_import(dash_item.function_path)
            return function(self.request, dash_item)

    def get_dash_dt(self, dash_item):
        tmp = {
            'is_active': False,
            'pk': dash_item.pk,
            'name': dash_item.name,
            'label': dash_item.label,
            'menu_label': self._get_dash_menu_label(dash_item),
            'desc': dash_item.description,
            'icon_class': dash_item.icon_class,
            'in_menu': dash_item.in_menu,
            'is_divider': dash_item.ttype == KdashK.DASH_TYPE_DIVIDER.value,
            'app_model': get_str_from_model(dash_item)
        }

        # perm_code = '%s.%s_%s' % ('kdash', KdashK.PERM_PREFIX.value, dash_item.name)
        # perm_code = f'kdash.{KdashK.PERM_PREFIX.value}_{dash_item._meta.model_name}|{dash_item.name}'
        perm_code = f'kdash.{get_perm_codename(dash_item)}'
        can_view = self.request.user.has_perm(perm_code)

        # if 'website' in perm_code.lower():
        #     pp

        dash_name = self.dash_name
        if can_view:
            tmp['url'] = self.get_dash_url(dash_item)
            tmp['can_view'] = can_view
            tmp['perm_code'] = perm_code

            if dash_item.get_compound_name() == dash_name:
                tmp['is_active'] = True
            else:
                tmp['is_active'] = False

            child_names = [x.get_compound_name() for x in dash_item.children.all()]
            if dash_name in child_names:
                tmp['is_parent_active'] = True
            else:
                tmp['is_parent_active'] = False

        else:
            tmp = {}

        return tmp

    def get_dash_url(self, dash_item):
        url = ''

        if dash_item.ttype == KdashK.DASH_TYPE_ADMINX.value:
            url = reverse('kdash_adminx:list', args=(dash_item.name,))

        elif dash_item.ttype == KdashK.DASH_TYPE_ADMINX_REPORT_LISTING.value:
            url = reverse('kdash_adminx:report_listing', args=(dash_item.name,))

        elif dash_item.ttype == KdashK.DASH_TYPE_ADMINX_REPORT_SUMMARY.value:
            url = reverse('kdash_adminx:report_summary', args=(dash_item.name,))

        elif dash_item.ttype == KdashK.DASH_TYPE_ADMINX_CENTRAL.value:
            url = reverse('kdash_adminx:central', args=(dash_item.name,))

        else:
            if dash_item.children.exists() or dash_item.ttype in [KdashK.DASH_TYPE_DIVIDER.value,
                                                                  KdashK.DASH_TYPE_CUSTOM_MENU_LABEL.value]:
                url = '#'

            elif dash_item.in_menu:
                url = reverse(dash_item.url_name)

        return url

    def get_quick_help(self):
        return {}
