
class DashboardItemAttr(object):   
    admin_list_display = ('name', 'label', 'description', 'in_menu', 'parent', 'content_str', 'icon_class', 'is_active',
                          'site')
    admin_fields = ('name', 'label', 'description', 'in_menu', 'parent', 'content_str', 'url_name', 'icon_class',
                    'site', 'function_path')
    admin_list_filter = ('content_str', 'in_menu', 'is_active', 'site')
