from enum import Enum


class KdashK(Enum):
    # PERM_PREFIX = 'can_view_dash'
    PERM_PREFIX = 'view'

    DASH_TYPE_DIVIDER = 'divider'
    DASH_TYPE_ADMINX = 'adminx'
    DASH_TYPE_ADMINX_CENTRAL = 'adminx_central' 
    DASH_TYPE_ADMINX_REPORT_LISTING = 'adminx_report_listing'
    DASH_TYPE_ADMINX_REPORT_SUMMARY = 'adminx_report_summary'
    DASH_TYPE_CUSTOM_MENU_LABEL = 'custom_menu_label'

    ADMIN_VIEW_CHANGE = 'change'
    ADMIN_VIEW_ADD = 'add'
    ADMIN_VIEW_LIST = 'list'
    ADMIN_VIEW_DELETE = 'delete'