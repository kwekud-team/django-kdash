from django.contrib import admin
from django.apps import apps
from django.shortcuts import get_object_or_404

from kdash.models import DashboardItem
from kdash.attrs import DashboardItemAttr
from kdash.utils import DashUtils
from kdash.constants import KdashK


@admin.register(DashboardItem)
class DashboardItemAdmin(admin.ModelAdmin):
    list_display = DashboardItemAttr.admin_list_display
    list_filter = DashboardItemAttr.admin_list_filter
    list_editable = ['site']
    fields = DashboardItemAttr.admin_fields


class KDashSite(admin.AdminSite):
    site_header = "Dash Admin"
    site_title = "Dash Admin Portal"
    index_title = "Welcome to Dash Portal"

    def _guess_model_class(self, request):
        url_parts = [x for x in request.path.split('/') if x]
        if len(url_parts) > 2:
            app_label, model_name = url_parts[1], url_parts[2]
            return apps.get_model(app_label, model_name)
        return None

    def _get_changelist_url(self, model_class):
        app_label = model_class._meta.app_label
        model_name = model_class._meta.model_name
        return '%s:%s_%s_changelist' % (self.name, app_label, model_name)

    def get_site(self, request):
        return request.site

    def each_context(self, request):
        ctx = super(KDashSite, self).each_context(request)

        model_class = self._guess_model_class(request)
        if model_class: 
            dash_name = self._get_changelist_url(model_class)
            site = self.get_site(request)

            dash_utils = DashUtils(request, site, dash_name=dash_name)
            ctx.update(dash_utils.get_context())

        return ctx


kdash_admin_site = KDashSite(name='kdashmin')


class FilterMixin:
    filter_class = None

    def get_filter_class(self, request):
        return self.filter_class

    def get_filter_form(self, request):
        filter_class = self.get_filter_class(request)
        if filter_class:
            return filter_class(request.GET, request=request).form


class KDashModelAdmin(admin.ModelAdmin, FilterMixin):
    change_form_template = 'kdashmin/change_form.html'
    change_list_template = 'kdashmin/change_list.html'
    delete_confirmation_template = 'kdashmin/delete_confirmation.html'
    delete_selected_confirmation_template = 'kdashmin/delete_selected_confirmation.html'

    def get_verbose_names(self, request, obj=None, extra_context=None):
        return {
            'custom_verbose_name_plural': '',
            'custom_verbose_name': ''
        } 

    def _get_extra_context(self, request, obj=None, extra_context=None):
        extra_context = extra_context or {}
        extra_context.update(self.get_verbose_names(request, obj=obj, extra_context=extra_context))
        return extra_context

    def add_view(self, request, form_url='', extra_context=None):
        extra_context = extra_context or {}
        extra_context['view_function'] = KdashK.ADMIN_VIEW_ADD.value
         
        extra_context = self._get_extra_context(request, extra_context=extra_context)

        # Set custom page title
        custom_verbose_name = extra_context.get('custom_verbose_name', '')
        if custom_verbose_name:
            extra_context['title'] = f'Add {custom_verbose_name}'

        return super(KDashModelAdmin, self).add_view(request, form_url=form_url, extra_context=extra_context)

    def change_view(self, request, object_id, form_url='', extra_context=None):
        extra_context = extra_context or {}
        extra_context['view_function'] = KdashK.ADMIN_VIEW_CHANGE.value

        obj = get_object_or_404(self.model, pk=object_id)
        extra_context = self._get_extra_context(request, obj=obj, extra_context=extra_context)

        # Set custom page title
        custom_verbose_name = extra_context.get('custom_verbose_name', '')
        if custom_verbose_name:
            extra_context['title'] = f'Change {custom_verbose_name}'

        return super(KDashModelAdmin, self).change_view(request, object_id, form_url=form_url,
                                                        extra_context=extra_context)

    def changelist_view(self, request, extra_context=None):
        extra_context = extra_context or {}
        extra_context['view_function'] = KdashK.ADMIN_VIEW_LIST.value
        extra_context['filter_form'] = self.get_filter_form(request)

        extra_context = self._get_extra_context(request, extra_context=extra_context)
        return super(KDashModelAdmin, self).changelist_view(request, extra_context=extra_context)
