from urllib import parse
from urllib.parse import urlparse
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import TemplateView
from django.http import HttpResponseRedirect

from kdash.utils import DashUtils
from kommons.utils.http import get_request_site
from kommons.views import ReferrerRedirectView


class BaseDashView(LoginRequiredMixin, TemplateView):
    dash_name = ''
    template_name = 'kdash/base.html'
    page_title = None
    page_subtitle = None
    dash_utils = None

    def get_dash_name(self):
        return self.dash_name

    def get_site(self):
        return get_request_site(self.request)

    # def get(self, request, *args, **kwargs):
    #     self.dash_utils = DashUtils(self.request, self.get_dash_name(), self.page_title, self.page_subtitle)
    #     return super().get(request, *args, **kwargs)

    def dispatch(self, request, *args, **kwargs):
        self.dash_utils = DashUtils(self.request, self.get_site(), self.get_dash_name(), self.page_title,
                                    self.page_subtitle)
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(BaseDashView, self).get_context_data(**kwargs)  

        # dash_utils = DashUtils(self.request, self.get_dash_name(), self.page_title, self.page_subtitle)
        context.update(self.dash_utils.get_context())

        return context


class ProxyFilterView(ReferrerRedirectView):
    referrer_key = 'kdash:proxy-view'

    def get(self, request, *args, **kwargs):
        query_dict = self.get_query_dict()
        params = self.flatten_params(query_dict)
        original_url = self.get_referrer()

        if original_url:
            parsed = parse.urlparse(original_url)
            full_url = f'{parsed.path}?{params}'
        else:
            full_url = '/'

        return HttpResponseRedirect(full_url)

    def get_query_dict(self):
        return {k: v for k, v in self.request.GET.dict().items() if v}

    def flatten_params(self, query_dict):
        return parse.urlencode(query_dict)
