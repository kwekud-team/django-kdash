from django.urls import path
from kdash import views

app_name = 'kdash'

urlpatterns = [
    path('proxy-filter/', views.ProxyFilterView.as_view(), name='proxy_filter'),
]
