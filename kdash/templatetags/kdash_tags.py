from django import template
from kdash.utils import DashUtils

register = template.Library()


@register.filter
def get_kdash_context(request, dash_name):
    dash_utils = DashUtils(request, dash_name=dash_name)
    return dash_utils.get_context()
