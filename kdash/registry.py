from kdash.models import DashboardItem
from kdash.utils import create_dash_perm
from kdash.constants import KdashK


_ITEMS = []


class Register:

    @staticmethod
    def add(name, label='', description='', content_str='', in_menu=True, url_name=None, sort=None, parent=None,
            icon_class='', ttype='regular', function_path=''):
        # app, model = self.content_str.split('.')
        url_name = url_name or name
        t = {
            'name': name,
            'label': label,
            'description': description, 
            'content_str': content_str, 
            'in_menu': in_menu,
            'url_name': url_name,
            'sort': sort,
            'parent': parent,
            'icon_class': icon_class,
            'ttype': ttype,
            'function_path': function_path,
        }
        if t not in _ITEMS:
            _ITEMS.append(t)

    @staticmethod
    def get_items():
        return _ITEMS

    @staticmethod
    def populate(site):
        DashboardItem.objects.filter(site=site).update(is_active=False)
        
        for pos, x in enumerate(_ITEMS):
            sort = x['sort'] or (pos + 100)
            parent = None
            if x['parent']:
                parent = DashboardItem.objects.get(site=site, name=x['parent'])

            DashboardItem.objects.update_or_create(
                site=site,
                content_str=x['content_str'],
                name=x['name'],
                ttype=x['ttype'],
                defaults={
                    'label': x['label'],
                    'description': x['description'],
                    'in_menu': x['in_menu'],
                    'url_name': x['url_name'],
                    'icon_class': x['icon_class'],
                    'function_path': x['function_path'],
                    'is_active': True,
                    'sort': sort,
                    'parent': parent,
                })

    @staticmethod
    def create_permissions(site):
        qs = DashboardItem.objects.filter(site=site, is_active=True)
        for x in qs:
            create_dash_perm(KdashK.PERM_PREFIX.value, x)
